window.onload = function(){
    document.getElementById("btn-keys").addEventListener("click", function(){
        const ERROR_MSG = "Cadena de llaves no balanceada."
        const CORRECT_MSG = "Cadena correcta"

        var text = document.getElementsByTagName("input")[0].value

        var isTextValid = /^(\{*\}*)+$/.test(text)

        var message = ""
        var isCorrect = true

        if(isTextValid && text !== ""){
            var keysStack = []

            stringIterator: 
            for(var i=0; i<text.length; i++){
                switch(text.charAt(i)){
                    case '{':
                        keysStack.push("{")
                        break;
                    case '}':
                        if(keysStack.pop() === undefined){
                            isCorrect = false
                            break stringIterator;
                        }

                        break;
                }
            }

            if(keysStack.length !== 0)
                isCorrect = false

            if(isCorrect)
                message = CORRECT_MSG
            else
                message = ERROR_MSG
        }else
            message = "Texto inválido"

        document.getElementsByTagName("p")[0].innerText = message
    });
} 